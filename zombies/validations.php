<?php

require_once "../app/config/config.php";
require_once "../app/models/Model.php";

$model = new Model;

/*
|--------------------------------------------------------------------------
| Perfil del usuario
|--------------------------------------------------------------------------
|
| Control de campos en perfil de usuario
|
*/

if (isset($_POST['user']))
{
	$nombre1 = (isset($_POST['nombre1'])) ? preg_replace('([^A-Za-zÁ-ź0-9 ])', '', trim($_POST['nombre1'])) : null;
	$nombre2 = (isset($_POST['nombre2'])) ? preg_replace('([^A-Za-zÁ-ź0-9 ])', '', trim($_POST['nombre2'])) : null;
	$apellido1 = (isset($_POST['apellido1'])) ? preg_replace('([^A-Za-zÁ-ź0-9 ])', '', trim($_POST['apellido1'])) : null;
	$apellido2 = (isset($_POST['apellido2'])) ? preg_replace('([^A-Za-zÁ-ź0-9 ])', '', trim($_POST['apellido2'])) : null;
	$region = (isset($_POST['region'])) ? preg_replace('([^A-Za-zÁ-ź0-9 ])', '', trim($_POST['region'])) : null;
	$cargo = (isset($_POST['cargo'])) ? preg_replace('([^A-Za-zÁ-ź0-9 ])', '', trim($_POST['cargo'])) : null;

	$_SESSION['log']['nombre1'] = (!is_null($nombre1)) ? $nombre1 : $_SESSION['log']['nombre1'];
	$_SESSION['log']['nombre2'] = (!is_null($nombre2)) ? $nombre2 : $_SESSION['log']['nombre2'];
	$_SESSION['log']['apellido1'] = (!is_null($apellido1)) ? $apellido1 : $_SESSION['log']['apellido1'];
	$_SESSION['log']['apellido2'] = (!is_null($apellido2)) ? $apellido2 : $_SESSION['log']['apellido2'];
	$_SESSION['log']['region'] = (!is_null($region)) ? $region : $_SESSION['log']['region'];
	$_SESSION['log']['cargo'] = (!is_null($cargo)) ? $cargo : $_SESSION['log']['cargo'];

	$name = $_SESSION['log']['nombre1']." ".$_SESSION['log']['nombre2'];
	$lastname = $_SESSION['log']['apellido1']." ".$_SESSION['log']['apellido2'];
	$_SESSION['log']['nombreCompleto'] = $name." ".$lastname;

}

/*
|--------------------------------------------------------------------------
| Registro de usuario
|--------------------------------------------------------------------------
|
| Comprobaciones de campos en registro local de usuario
|
*/

if (isset($_POST['register']))
{
	$abc = 'ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyzáéíóúÁÉÍÓÚ ';
	$abc = str_split($abc);

	$mail_array = 'abcdefghijklmnñopqrstuvwxyz1234567890_.-@';
	$mail_array = str_split($mail_array);

	if (isset($_POST['nombre1'])) { $string = trim($_POST['nombre1']); $_SESSION['dataRegister']['nombre1'] = $string; }
	if (isset($_POST['nombre2'])) { $string = trim($_POST['nombre2']); $_SESSION['dataRegister']['nombre2'] = $string; }
	if (isset($_POST['apellido1'])) { $string = trim($_POST['apellido1']); $_SESSION['dataRegister']['apellido1'] = $string; }
	if (isset($_POST['apellido2'])) { $string = trim($_POST['apellido2']); $_SESSION['dataRegister']['apellido2'] = $string; }
	if (isset($_POST['cargo'])) { $string = trim($_POST['cargo']); $_SESSION['dataRegister']['cargo'] = $string; }
	if (isset($_POST['email'])) { $email = strtolower($_POST['email']); $_SESSION['dataRegister']['email'] = $email; }

	if (isset($string)) {
		$string = str_split($string);
		foreach ($string as $value) { if (in_array($value, $abc)) { $res = true; }else { $res = false; break; } }
	}

	if (isset($email)) {
		$mail = str_split($email);
		foreach ($mail as $value) { if (in_array($value, $mail_array)) { $res = true; }else { $res = false; break; } }
		if ($res) {
			$arroba = strpos($email, '@');
			if (!empty($arroba)) {
				$dominio = substr($email, $arroba);
				$punto = strrpos($dominio, '.');
				$res = (!empty($punto)) ? true : false;
			}else {
				$res = false;
			}
		}
	}

	echo $res;
}