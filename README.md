# thisFramework 🗂️

"thisFramework" is a personal project for those who are starting in the world of software development and want to refine their knowledge of object-oriented programming, library use, etc.

If you know about good development practices and you find one or many bugs, errors or bad practices that need to be modified or eliminated, I would greatly appreciate your help to improve this project.

> If you want to use this framework for your personal projects, feel free to do so and adhere to the terms established in the [LICENSE](LICENSE) file.

## How to use it 🛠️

* Download the repository at the root of your projects.
* Edit the config.php file in "app/config/" and set your database parameters, project URL, etc.
* Edit the .htaccess file and set the modified URL in config.php.
* Execute the .sql files found in "app/config/data/".
* Configure a scheduled task for the tasks.php file hosted in "zombies/this/".

## Mailing Settings 📬

### open the php.ini file.

* Find [mail function] by pressing ctrl + f.
Search and pass the following values:
```
SMTP=smtp.gmail.com
smtp_port=587
sendmail_from = YourGmailId@gmail.com
```
In XAMPP
```
sendmail_path = "\"C:\xampp\sendmail\sendmail.exe\" -t"
```
In Linux
```
sendmail_path = /usr/sbin/sendmail -t
```

## License 📑

thisFramework is open source software under the GPL v3.0 license. Please see full terms in the [LICENSE](LICENSE) file.