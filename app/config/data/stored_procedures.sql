/*
-----------------------------------------
|       PROCEDIMIENTOS ALMACENADOS      |
-----------------------------------------
*/

USE `db_thisframework`;

DELIMITER //
CREATE PROCEDURE SP_login( _id INT )
BEGIN
	SELECT
		u.idusuario,
		n.nivelusuario AS 'nivel'
	FROM
		tbl_usuarios u
	INNER JOIN
		tbl_nivelesusuarios n ON u.idnivelusuario = n.idnivelusuario
	WHERE
		u.idusuario = _id AND idEstado = 1;
END //


DELIMITER //
CREATE PROCEDURE SP_infousuario( _id INT )
BEGIN
	SELECT
		u.idusuario,
		u.firstname,
        u.secndname,
        u.firstape,
        u.secndape,
        u.email,
		n.nivelusuario AS 'Nivel',
        u.cargo,
        f.foto,
        e.estado
	FROM
		tbl_usuarios u
	INNER JOIN
		tbl_nivelesusuarios n ON u.idnivelusuario = n.idnivelusuario
	INNER JOIN
		tbl_fotoperfiles f ON u.idfoto = f.idfoto
	INNER JOIN
		tbl_estados e ON u.idestado = e.idestado
	WHERE
		u.idusuario = _id;
END //


DELIMITER //
CREATE PROCEDURE SP_actualizarusuario(
    _firstname 	VARCHAR(45),
    _secndname 	VARCHAR(45),
    _firstape 	VARCHAR(45),
    _secndape 	VARCHAR(45),
    _cargo		VARCHAR(70),
    _idusuario	INT
)
BEGIN
	UPDATE
		tbl_usuarios
	SET
		firstname = _firstname,
        secndname = _secndname,
        firstape = _firstape,
        secndape = _secndape,
        cargo = _cargo
	WHERE
		idusuario = _idusuario;
END //


DELIMITER //
CREATE PROCEDURE SP_listausuarios()
BEGIN
	SELECT
		u.idusuario,
		u.firstname AS 'nombre1',
		u.secndname  AS 'nombre2',
		u.firstape  AS 'apellido1',
		u.secndape  AS 'apellido2',
        CONCAT(u.firstname, ' ', u.secndname, ' ', u.firstape, ' ', u.secndape) AS 'nombreCompleto',
		u.email,
		u.cargo,
		u.idestado,
        e.estado
	FROM
		tbl_usuarios u
	INNER JOIN
		tbl_estados e ON u.idestado = e.idestado;
END //


DELIMITER //
CREATE PROCEDURE SP_useregister(
	_nombre1	VARCHAR(45),
	_nombre2	VARCHAR(45),
	_apellido1	VARCHAR(45),
	_apellido2	VARCHAR(45),
	_email		VARCHAR(70),
	_pass		VARCHAR(100),
	_cargo		VARCHAR(70)
)
BEGIN
	INSERT INTO
		`tbl_usuarios`
	VALUES
		(NULL, _nombre1, _nombre2, _apellido1, _apellido2, 3, _email, _pass, _cargo, NULL, NULL, 0, 1, 3);
END //