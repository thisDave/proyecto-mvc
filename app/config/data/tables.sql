/*
----------------------------------------
|         CREACIÓN Y SELECCION         |
----------------------------------------
*/

DROP DATABASE IF EXISTS `db_thisframework`;

CREATE DATABASE IF NOT EXISTS `db_thisframework` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

USE `db_thisframework`;

/*
----------------------------------------
|          TABLAS PRINCIPALES          |
----------------------------------------
*/


/*TABLA Niveles de Usuario*/

CREATE TABLE IF NOT EXISTS tbl_nivelesusuarios(
  idnivelusuario	INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  nivelusuario		VARCHAR(20) NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* TABLA Estados */

CREATE TABLE IF NOT EXISTS tbl_estados(
	idestado	INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	estado		VARCHAR(25) NOT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*TABLA Fotos de Perfil*/

CREATE TABLE IF NOT EXISTS tbl_fotoperfiles(
	idfoto		INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	nombre		VARCHAR(15) NOT NULL,
	formato		VARCHAR(12) NOT NULL,
	foto 		BLOB NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*TABLA Usuarios*/

CREATE TABLE IF NOT EXISTS tbl_usuarios(
	idusuario		INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
	firstname 		VARCHAR(45) NOT NULL,
    secndname		VARCHAR(45) NOT NULL,
	firstape 		VARCHAR(45) NOT NULL,
    secndape		VARCHAR(45) NOT NULL,
	idNivelusuario 	INT NOT NULL,
	email 			VARCHAR(70) NOT NULL,
	pass			VARCHAR(100) NOT NULL,
	cargo			VARCHAR(70) NOT NULL,
	token 			VARCHAR(70) NULL,
	fechatoken 		DATETIME NULL,
	mailRegister	INT NOT NULL DEFAULT 0,
    idfoto			INT NOT NULL DEFAULT 1,
    idestado		INT NOT NULL,

	CONSTRAINT FK_usuarios_idnivelusuario FOREIGN KEY (idnivelusuario) REFERENCES tbl_nivelesusuarios (idnivelusuario),

    CONSTRAINT FK_usuarios_idfoto FOREIGN KEY (idfoto) REFERENCES tbl_fotoperfiles (idfoto),

    CONSTRAINT FK_usuarios_idestado FOREIGN KEY (idestado) REFERENCES tbl_estados (idestado)

)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*TABLA Cookies*/

CREATE TABLE IF NOT EXISTS tbl_cookies(
	email			VARCHAR(60) NOT NULL,
	pass			VARCHAR(60) NOT NULL,
    sessiontoken	VARCHAR(125) NOT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/*TABLA Comentarios*/

CREATE TABLE IF NOT EXISTS tbl_comentariosistema(
	idcomentario	INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    idusuario		INT NULL,
    comentario		VARCHAR(200),
    fecha			DATE,
    hora			TIME
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Registros de entradas */

CREATE TABLE IF NOT EXISTS tbl_entradas(
	identrada	INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    idusuario	INT NOT NULL,
    fecha		DATETIME NOT NULL DEFAULT NOW(),

    CONSTRAINT FK_entradas_idusuario FOREIGN KEY (idusuario) REFERENCES tbl_usuarios (idusuario)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* Registros de salidas */

CREATE TABLE IF NOT EXISTS tbl_salidas(
	identrada	INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    idusuario	INT NOT NULL,
    fecha		DATETIME NOT NULL DEFAULT NOW(),

    CONSTRAINT FK_salidas_idusuario FOREIGN KEY (idusuario) REFERENCES tbl_usuarios (idusuario)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* Registros de cron.php */

CREATE TABLE IF NOT EXISTS tbl_logscron(
	idlog	INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    idEstado INT NOT NULL,
    mensaje	VARCHAR(100) NOT NULL,

    CONSTRAINT FK_logscron_idestado FOREIGN KEY (idestado) REFERENCES tbl_estados (idestado)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;