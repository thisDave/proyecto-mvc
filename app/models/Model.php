<?php

require_once APP.'/config/Conection.php';

class Model extends Conection
{
	public function showRes($query)
	{
		$c = parent::conectar();
		$c->set_charset('utf8');
		$res = $c->query($query);
        parent::desconectar();
		return $res;
	}

	public function logInfo($email, $pass)
	{
		$resultado = $this->showRes("SELECT * FROM tbl_usuarios WHERE email = '{$email}' AND idEstado = 1");

        if ($resultado->num_rows > 0)
        {
            $fila = $resultado->fetch_assoc();

    	    if (password_verify($pass, $fila['pass']))
    	    	$id = $fila['idusuario'];
            else
                $id = false;

    		if ($id)
    		{
    			$conexion = parent::conectar();
    			$conexion->set_charset('utf8');
    			$stm = $conexion->prepare("call SP_login(?);");
    			$stm->bind_param('i', $id);
    			$stm->execute();

       			$stm->bind_result(
       				$idusuario,
    		        $nivel
       			);

    			if (!empty($stm))
    			{
    				while($stm->fetch())
    				{
                        $resultado = $this->showRes("SELECT * FROM `tbl_entradas` WHERE idusuario = {$idusuario}");

                        if ($resultado->num_rows < 1)
                        {
                            return 'firstIn';
                        }
                        else
                        {
        					$this->showRes("INSERT INTO `tbl_entradas`(`idusuario`) VALUES ({$idusuario})");

                            $_SESSION['log'] = $this->infoUsuario($idusuario);
                        }
    				}

    				$stm->close();

    				return true;
    			}
    			else
    			{
    				$stm->close();

    				return false;
    			}
    		}
    		else
    		{
    			return false;
    		}
        }
        else
        {
            return false;
        }
	}

    public function salida($id)
    {
        $this->showRes("INSERT INTO `tbl_salidas`(`idUsuario`) VALUES ({$id})");
    }

	public function infoUsuario($idUsuario)
    {
    	$resultado = $this->showRes("CALL SP_infousuario(".$idUsuario.")");

    	if ($resultado->num_rows > 0)
		{
			$info = [];

			while ($fila = $resultado->fetch_assoc())
			{
                $info['id'] = $fila['idusuario'];
				$info['nombre1'] = $fila['firstname'];
				$info['nombre2'] = $fila['secndname'];
                $info['apellido1'] = $fila['firstape'];
                $info['apellido2'] = $fila['secndape'];
				$info['email'] = $fila['email'];
				$info['level'] = $fila['Nivel'];
				$info['cargo'] = $fila['cargo'];
                $info['foto'] = base64_encode($fila['foto']);
                $info['estado'] = $fila['estado'];
			}

            $info['nombreCompleto'] = $info['nombre1']." ".$info['nombre2']." ".$info['apellido1']." ".$info['apellido2'];

			return $info;
		}
		else
		{
			return false;
		}

		$resultado->free();

		parent::desconectar();
    }

    public function actualizarUsuario()
    {
        $id = $_SESSION['log']['id'];
        $nombre1 = $_SESSION['log']['nombre1'];
        $nombre2 = $_SESSION['log']['nombre2'];
        $apellido1 = $_SESSION['log']['apellido1'];
        $apellido2 = $_SESSION['log']['apellido2'];
        $cargo = $_SESSION['log']['cargo'];

        $query = "CALL SP_actualizarusuario('{$nombre1}', '{$nombre2}', '{$apellido1}', '{$apellido2}', '{$cargo}', $id)";

    	$resultado = $this->showRes($query);

    	if ($resultado)
    		return true;
    	else
    		return false;
    }

	public function setCookieToken($email, $pass, $token)
	{
		$resultado = $this->showRes("INSERT INTO tbl_cookies VALUES ('{$email}', '{$pass}', '{$token}')");

		if($resultado)
			return true;
		else
			return false;
	}

	public function getCookieToken($token)
	{
		$resultado = $this->showRes("SELECT email, pass FROM tbl_cookies WHERE sessionToken = '{$token}'");

		$info = [];

		while ($campo = $resultado->fetch_assoc())
		{
		    $info['usuario'] = $campo['email'];
		    $info['pass'] = $campo['pass'];
		}

		if($resultado)
			return $info;
		else
			return false;
	}

	public function setResetToken($email, $token)
	{
        $now = date('Y-m-d');
		$resultado = $this->showRes("UPDATE tbl_usuarios SET token = '{$token}', fechaToken = '{$now}' WHERE email = '{$email}' AND idEstado = 1");
        $res = ($resultado) ? true : false;
		return $res;
	}

	public function validarToken($token)
	{
		if (strlen($token) == 50)
		{
			$resultado = $this->showRes("SELECT * FROM tbl_usuarios WHERE token = '{$token}'");

			if ($resultado->num_rows > 0)
				return true;
			else
				return false;
		}
	}

	public function recoverPassword($password, $token)
	{
        $res = $this->showRes("SELECT idusuario FROM tbl_usuarios WHERE token = '{$token}'");

        if ($res->num_rows > 0)
        {
            $dato = $res->fetch_assoc();

            $id = $dato['idusuario'];

            $this->showRes("INSERT INTO `tbl_entradas`(`idUsuario`) VALUES ({$id})");

            $token = $this->showRes("
                UPDATE
                    tbl_usuarios
                SET
                    fechatoken = NULL,
                    pass = '{$password}',
                    token = NULL,
                    idEstado = 1
                WHERE
                    idusuario = {$id}");

    		if ($token)
    			return true;
    		else
    			return false;
        }
        else
        {
            return false;
        }
	}

	public function validaPassword($currentPass)
	{
		$resultado = $this->showRes("SELECT * FROM tbl_usuarios");

		$x = false;

		while ($fila = $resultado->fetch_assoc())
		{
		    if ($fila['idusuario'] == $_SESSION['log']['id'] && password_verify($currentPass, $fila['pass']))
		    {
		    	$x = true;
		    	break;
		    }
		}

		return $x;
	}

	public function updatePassword($password)
	{
		$query = "
		UPDATE
			tbl_usuarios
		SET
			pass = '".$password."'
		WHERE
			idusuario = ".$_SESSION['log']['id'];

		$resultado = $this->showRes($query);

		if ($resultado)
			return true;
		else
			return false;
	}

    public function profilePics()
    {
        $resultado = $this->showRes("SELECT * FROM tbl_fotoperfiles");

        if ($resultado)
        {
            $fotos = [];

            while ($data = $resultado->fetch_assoc())
            {
                $fotos['idFoto'][] = $data['idfoto'];
                $fotos['nombre'][] = $data['nombre'];
                $fotos['formato'][] = $data['formato'];
                $fotos['foto'][] = base64_encode($data['foto']);
            }

            return $fotos;
        }
        else
        {
            return false;
        }
    }

    public function updtPicProfile($idFoto)
    {
        $resultado = $this->showRes("UPDATE tbl_usuarios SET idFoto = {$idFoto} WHERE idUsuario = {$_SESSION['log']['id']}");

        if ($resultado)
            return true;
        else
            return false;
    }

    public function listarEstados()
    {
        $resultado = $this->showRes("SELECT * FROM tbl_Estados");

        if ($resultado->num_rows > 0)
        {
            while($data = $resultado->fetch_assoc())
            {
                $estados['idEstado'][] = $data['idestado'];
                $estados['estado'][] = $data['estado'];
            }

            return $estados;
        }
        else
        {
            return false;
        }
    }

    public function listaUsuarios()
    {
        $resultado = $this->showRes("CALL SP_listausuarios()");

        if ($resultado)
        {
            if ($resultado->num_rows > 0)
            {
                while ($data = $resultado->fetch_assoc())
                {
                    $lista['idUsuario'][] = $data['idusuario'];
                    $lista['nombre1'][] = $data['nombre1'];
                    $lista['nombre2'][] = $data['nombre2'];
                    $lista['apellido1'][] = $data['apellido1'];
                    $lista['apellido2'][] = $data['apellido2'];
                    $lista['nombreCompleto'][] = $data['nombreCompleto'];
                    $lista['email'][] = $data['email'];
                    $lista['cargo'][] = $data['cargo'];
                    $lista['idEstado'][] = $data['idestado'];
                    $lista['estado'][] = $data['estado'];
                }

                return $lista;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public function insertComment($comment, $idUsuario)
    {
        if (!empty($comment))
        {
            $query = "INSERT INTO tbl_comentariosistema VALUES (NULL, {$idUsuario}, '{$comment}', CURDATE(), TIME_FORMAT(NOW(), '%H:%i'))";
            $resultado = $this->showRes($query);
        }
    }

    public function delComment($idComment, $idUsuario)
    {
        $res = $this->showRes("SELECT * FROM tbl_comentariosistema WHERE idcomentario = {$idComment} AND idUsuario = {$idUsuario}");

        if ($res && $res->num_rows > 0)
        {
            $resultado = $this->showRes("DELETE FROM tbl_comentariosistema WHERE idcomentario = {$idComment}");
        }
    }

    public function getComments()
    {
        $resultado = $this->showRes("SELECT * FROM tbl_comentariosistema ORDER BY idcomentario DESC");

        if ($resultado)
        {
            if ($resultado->num_rows > 0)
            {
                while ($data = $resultado->fetch_assoc())
                {
                    $comments['idComentario'][] = $data['idcomentario'];
                    $comments['idUsuario'][] = $data['idusuario'];
                    $comments['comentario'][] = $data['comentario'];
                    $comments['fecha'][] = $data['fecha'];
                    $comments['hora'][] = $data['hora'];
                }

                return $comments;
            }
            else
            {
                return false;
            }
        }
    }

    public function available_mail($email)
    {
        $res = $this->showRes("SELECT * FROM tbl_usuarios WHERE email = '{$email}'");
        return ($res->num_rows > 0) ? false : true;
    }

    public function userRegister($pwd)
    {
        $nombre1 = $_SESSION['dataRegister']['nombre1'];
        $email = $_SESSION['dataRegister']['email'];
        $nombre2 = $_SESSION['dataRegister']['nombre2'];
        $apellido1 = $_SESSION['dataRegister']['apellido1'];
        $apellido2 = $_SESSION['dataRegister']['apellido2'];
        $cargo = $_SESSION['dataRegister']['cargo'];

        unset($_SESSION['dataRegister']);

        $query = "CALL SP_useregister('{$nombre1}', '{$nombre2}', '{$apellido1}', '{$apellido2}', '{$email}', '{$pwd}', '{$cargo}')";

        $res = $this->showRes($query);

        return ($res) ? true : false;
    }

    protected function pending_mails()
    {
        $res = $this->showRes("SELECT * FROM tbl_usuarios WHERE mailRegister = 0 AND idEstado = 3");

        if ($res->num_rows > 0)
        {
            $userdata = [];
            if ($res) {
                while ($data = $res->fetch_assoc())
                {
                    $userdata['id'][] = $data['idusuario'];
                    $userdata['nombre1'][] = $data['firstname'];
                    $userdata['nombre2'][] = $data['secndname'];
                    $userdata['apellido1'][] = $data['firstape'];
                    $userdata['apellido2'][] = $data['secndape'];
                    $userdata['email'][] = $data['email'];
                }
            }
            return $userdata;
        }
        else
        {
            return false;
        }
    }

    protected function mails_sent($id, $token)
    {
        $now = date('Y-m-d');
        if ($this->showRes("UPDATE tbl_usuarios SET token = '{$token}', fechaToken = '{$now}', mailRegister = 1 WHERE idusuario = {$id}"))
            return true;
        else
            return false;
    }

    protected function delRegister($token)
    {
        $this->showRes("DELETE FROM tbl_usuarios WHERE token = '{$token}'");
    }

    protected function savelog($id, $mensaje)
    {
        $this->showRes("INSERT INTO tbl_logscron VALUES (NULL, {$id}, '{$mensaje}')");
    }
}