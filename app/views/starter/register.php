<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" type="image/x-icon" href="dist/img/icono.ico">

  <title><?= APP_NAME ?> | Registration Page</title>

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <link rel="stylesheet" href="plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="login-logo">
    <img src="dist/img/logo.png" class="h-50 w-50"><br>
    <a href="<?= URL ?>"><?= APP_NAME ?></a>
  </div>
  <!-- /.login-logo -->

  <div class="card">
    <div class="card-body register-card-body">
      <p class="login-box-msg">Registrarse en <?= APP_NAME ?></p>

      <?php if (LOCAL_LOGIN == true): ?>

      <form action="<?= URL ?>" method="post">
        <div class="input-group mb-3">
          <?php $val = (isset($_SESSION['dataRegister']['nombre1'])) ? $_SESSION['dataRegister']['nombre1'] : ''; ?>
          <input type="text" class="form-control" name="nombre1" id="nombre1" placeholder="Primer nombre" value="<?= $val ?>" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <?php $val = (isset($_SESSION['dataRegister']['nombre2'])) ? $_SESSION['dataRegister']['nombre2'] : ''; ?>
          <input type="text" class="form-control" name="nombre2" id="nombre2" placeholder="Segundo nombre" value="<?= $val ?>">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <?php $val = (isset($_SESSION['dataRegister']['apellido1'])) ? $_SESSION['dataRegister']['apellido1'] : ''; ?>
          <input type="text" class="form-control" name="apellido1" id="apellido1" placeholder="Primer apellido" value="<?= $val ?>" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <?php $val = (isset($_SESSION['dataRegister']['apellido2'])) ? $_SESSION['dataRegister']['apellido2'] : ''; ?>
          <input type="text" class="form-control" name="apellido2" id="apellido2" placeholder="Segundo apellido" value="<?= $val ?>">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <?php $val = (isset($_SESSION['dataRegister']['cargo'])) ? $_SESSION['dataRegister']['cargo'] : ''; ?>
          <input type="text" class="form-control" name="cargo" id="cargo" placeholder="Cargo en la empresa" value="<?= $val ?>">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <?php $val = (isset($_SESSION['dataRegister']['email'])) ? $_SESSION['dataRegister']['email'] : ''; ?>
          <input type="email" class="form-control" name="email" id="email" placeholder="Correo electrónico" value="<?= $val ?>" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <button type="submit" name="newregister" class="btn btn-success btn-block">Registrarse</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <?php endif ?>

      <?php if (
        FACEBOOK_LOGIN == true ||
        GOOGLE_LOGIN == true ||
        TWITTER_LOGIN == true ||
        MICROSOFT_LOGIN == true ||
        GITHUB_LOGIN == true): ?>

      <div class="social-auth-links text-center mb-3">

        <?php if (LOCAL_LOGIN == true): ?>
        <p>- O Puedes -</p>
        <?php endif ?>

        <?php if (FACEBOOK_LOGIN == true): ?>
        <a href="#" class="btn btn-primary btn-block btn-flat">
          <i class="fab fa-facebook-f mr-2"></i> Sign up with Facebook
        </a>
        <?php endif ?>

        <?php if (GOOGLE_LOGIN == true): ?>
        <a href="#" class="btn btn-danger btn-block btn-flat">
          <i class="fab fa-google mr-2"></i> Sign up with Google
        </a>
        <?php endif ?>

        <?php if (MICROSOFT_LOGIN == true): ?>
        <a href="#" class="btn btn-dark btn-block btn-flat">
          <i class="fab fa-windows mr-2"></i> Sign up with Microsoft
        </a>
        <?php endif ?>

        <?php if (TWITTER_LOGIN == true): ?>
        <a href="#" class="btn btn-info btn-block btn-flat">
          <i class="fab fa-twitter mr-2"></i> Sign up with Twitter
        </a>
        <?php endif ?>

        <?php if (GITHUB_LOGIN == true): ?>
        <a href="#" class="btn btn-dark btn-block btn-flat">
          <i class="fab fa-github mr-2"></i> Sign up with github
        </a>
        <?php endif ?>

      </div>
      <!-- /.social-auth-links -->

      <?php endif ?>

      ¿Ya te encuentras registrado?<a href="<?= URL ?>?action=login" class="text-center"> Inicia sesión aquí</a>
    </div>
    <!-- /.form-box -->
  </div><!-- /.card -->
</div>
<!-- /.register-box -->

<script src="plugins/jquery/jquery.min.js"></script>
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="dist/js/adminlte.min.js"></script>
<script src="plugins/sweetalert2/sweetalert2.min.js"></script>
<script src="dist/js/register.js"></script>

<?= $objController->sweetAlert(5000); ?>

</body>
</html>