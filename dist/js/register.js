$(document).ready(function () {

	$("#nombre1").keyup(function() {
		var nombre1 = $("#nombre1").val();
		var ruta = "register=&nombre1="+nombre1;
		$.ajax({
			type: 'post',
			url: 'data',
			data: ruta
		})
		.done(function(res){
			if (res) {
				$("#nombre1").removeClass('is-invalid');
				$("#nombre1").addClass('is-valid');
			}else {
				$("#nombre1").removeClass('is-valid');
				$("#nombre1").addClass('is-invalid');
			}
		});
	});

	$("#nombre2").keyup(function() {
		var nombre2 = $("#nombre2").val();
		var ruta = "register=&nombre2="+nombre2;
		$.ajax({
			type: 'post',
			url: 'data',
			data: ruta
		});
	});

	$("#apellido1").keyup(function() {
		var apellido1 = $("#apellido1").val();
		var ruta = "register=&apellido1="+apellido1;
		$.ajax({
			type: 'post',
			url: 'data',
			data: ruta
		})
		.done(function(res){
			if (res) {
				$("#apellido1").removeClass('is-invalid');
				$("#apellido1").addClass('is-valid');
			}else {
				$("#apellido1").removeClass('is-valid');
				$("#apellido1").addClass('is-invalid');
			}
		});
	});

	$("#apellido2").keyup(function() {
		var apellido2 = $("#apellido2").val();
		var ruta = "register=&apellido2="+apellido2;
		$.ajax({
			type: 'post',
			url: 'data',
			data: ruta
		});
	});

	$("#cargo").keyup(function() {
		var cargo = $("#cargo").val();
		var ruta = "register=&cargo="+cargo;
		$.ajax({
			type: 'post',
			url: 'data',
			data: ruta
		});
	});

	$("#email").keyup(function() {
		var email = $("#email").val();
		var ruta = "register=&email="+email;
		$.ajax({
			type: 'post',
			url: 'data',
			data: ruta
		})
		.done(function(res){
			if (res) {
				$("#email").removeClass('is-invalid');
				$("#email").addClass('is-valid');
			}else {
				$("#email").removeClass('is-valid');
				$("#email").addClass('is-invalid');
			}
		});
	});
});